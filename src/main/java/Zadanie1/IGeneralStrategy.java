package Zadanie1;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public interface IGeneralStrategy {

    List<AbstractSoldier> generateSoldiers(int experiencePoints);
    void fight(IGeneralStrategy oponentGeneral, int numberOfMoves);
    List<AbstractSoldier> getArmy();
    void buryTheCorpses();

}
