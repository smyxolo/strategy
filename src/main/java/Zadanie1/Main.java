package Zadanie1;

public class Main {

    public static void main(String[] args) {
        LeftSideGeneral l = new LeftSideGeneral(20);
        RightSideGeneral r = new RightSideGeneral(10);

        Arena arena = new Arena(l, r);
        arena.war();
    }
}
