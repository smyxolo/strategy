package Zadanie1;

public class KnightSoldier extends AbstractSoldier {

    public KnightSoldier(int experience_points) {
        super(experience_points);
        this.archeryDefenceLevel = 7;
        this.fightDefenceLevel = 10;
        this.spellDefenceLevel = 8;
        this.soldier_type = SOLDIER_TYPE.KNIGHT;
    }
}
