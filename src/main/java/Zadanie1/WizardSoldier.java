package Zadanie1;

public class WizardSoldier extends AbstractSoldier {


    public WizardSoldier(int experience_points) {
        super(experience_points);
        this.archeryDefenceLevel = 8;
        this.fightDefenceLevel = 7;
        this.spellDefenceLevel = 10;
        this.soldier_type = SOLDIER_TYPE.WIZARD;
    }
}
