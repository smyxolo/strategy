package Zadanie1;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Getter
@Setter

public class LeftSideGeneral implements IGeneralStrategy {
    int experienceGathered = 1000;
    List<AbstractSoldier> army;

    public LeftSideGeneral(int experiencePerSoldier) {
        this.army = generateSoldiers(experiencePerSoldier);
    }

    @Override
    public List<AbstractSoldier> generateSoldiers(int experiencePoints) {
        Random random = new Random();
        List<AbstractSoldier> soldierList = new ArrayList<>();
        for (int i = 0; i < experienceGathered/experiencePoints; i++) {
            int randomSoldier = random.nextInt(3);
            switch (randomSoldier){
                case 0: {
                    soldierList.add(new ArcherSoldier(experiencePoints));
                    break;
                }

                case 1: {
                    soldierList.add(new KnightSoldier(experiencePoints));
                    break;
                }

                case 2: {
                    soldierList.add(new WizardSoldier(experiencePoints));
                    break;
                }
            }
        }
        return soldierList;
    }

    public void fight(IGeneralStrategy oponentGeneral, int numberOfMoves) {
        Random random = new Random();
        for (int i = 0; i < numberOfMoves; i++) {
            if(army.size() == 0 || oponentGeneral.getArmy().size() == 0) throw new AllDeadException("Army defeated.");
            army.get(random.nextInt(army.size()))
                    .fight(oponentGeneral.getArmy().get(random.nextInt(oponentGeneral.getArmy().size())));

        }
        buryTheCorpses();
        oponentGeneral.buryTheCorpses();
    }

    public void buryTheCorpses(){
        List<AbstractSoldier> corpses = new ArrayList<>();

        for (AbstractSoldier soldier : army) {
            if(soldier.health_points <= 0) corpses.add(soldier);
        }
        army.removeAll(corpses);
    }

    @Override
    public List<AbstractSoldier> getArmy() {
        return army;
    }
}
