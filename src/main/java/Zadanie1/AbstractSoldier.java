package Zadanie1;

import lombok.Getter;

@Getter

public class AbstractSoldier {
    int experience_points;
    double health_points = 1000.0;
    int archeryDefenceLevel;
    int fightDefenceLevel;
    int spellDefenceLevel;
    SOLDIER_TYPE soldier_type;

    /*
    (ilość_doświadczenia/10.0) - (level_obrony_TYP_przeciwnika*2.0*jego_ilosc_doswiadczenia)

    np. gdy walczy łucznik(100exp) (atakuje) kontra czarodziej(50exp):

    zabieramy   = (100 / 10.0) - (8 * 2.0 * 50) = 1000 - 800 = 200;
        ^ expo*/

    public AbstractSoldier(int experience_points) {
        this.experience_points = experience_points;
    }

    public boolean fight(AbstractSoldier soldierToFight) {
        int defence = getDefenceAgainstOpponent(soldierToFight);
        double totalDamage;
        if((experience_points / 10) - (defence * 2.0 * soldierToFight.experience_points) > 5){
            totalDamage = (experience_points / 10) - (defence * 2.0 * soldierToFight.experience_points);
        }
        else totalDamage = 5;

        double opponentHealth = soldierToFight.health_points;

        if(opponentHealth > 0){
            if(opponentHealth >= totalDamage) soldierToFight.health_points -= totalDamage;
            else if (opponentHealth < totalDamage) soldierToFight.health_points = 0;
        }
        return (soldierToFight.health_points == 0);
    }

    private int getDefenceAgainstOpponent(AbstractSoldier soldierToFight) {
        int opponentDefence = 0;
        switch (soldierToFight.soldier_type) {
            case ARCHER: {
                opponentDefence = archeryDefenceLevel;
                break;
            }
            case KNIGHT: {
                opponentDefence = fightDefenceLevel;
                break;
            }
            case WIZARD: {
                opponentDefence = spellDefenceLevel;
                break;
            }
        }
        return opponentDefence;
    }
}
