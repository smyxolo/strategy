DesignPatternStrategyHeroes
Stworzymy aplikację gdzie będziemy mieli do dyspozycji dwa mechanizmy strategii.
1. Stwórz klasę abstrakcyjną AbstractSoldier
    - posiada ilość doświadczenia (experience_points)
    - posiada ilość życia (health_points) - domyślnie na początku 1000.0
    - posiada level obrony przed łukiem
    - posiada level obrony przed walką wręcz
    - posiada level obrony przed czarami

    - posiada metodę:
    public boolean fight(AbstractSoldier soldierToFight);
walka powinna odbierać życia w zalezności od tego z jakim żołnierzem walczymy i jakim jesteśmy.
odbieramy ilość życia (wzór):
    (ilość_doświadczenia/10.0) - (level_obrony_TYP_przeciwnika*2.0*jego_ilosc_doswiadczenia)
np. gdy walczy łucznik(100exp) (atakuje) kontra czarodziej(50exp):
zabieramy   = (100 * 10.0) - (8 * 2.0 * 50) = 1000 - 800 = 200;
        ^ expo,          ^obrona maga przed łucznikiem  * 2.0 * experience
nie możemy zabrać ujemnej liczby życia, ale zabieramy minimum 5 pkt życia.
- walka z podanym żołnierzem. zwraca true/false (true jeśli zabił przeciwnika)
2. Stwórz klasy dziedziczące:
    ArcherSoldier level obrony przed łukiem 10, obrony przed wręcz 8, obrony przed czarami 7
    KnightSoldier level obrony przed walką wręcz 10, obrony przed czarami 8, obrony przed łukiem 7
    MageSoldier   level obrony przed czarami 10, obrony przed łukiem 8, obrony przed wręcz 7
3. Mechanizm generała:
    -- stwórz interfejs IGeneralStrategy który posiada metodę wytwarzania żołnierzy. metoda:
public List<AbstractSoldier> generateSoldier(int experiencePoints);
    - metoda tworzy żołnierzy. Ma do wykorzystania ilość punktów doświadczenia. Z danych punktów może stworzyć tylko tyle żołnierzy, aby użyć wszystkich pkt experience. np. może stworzyć 1000 ludzi o 1exp lub 100 ludzi z 10exp lub 1 ludzika z 1000 exp.

    - metoda fight(IGeneralStrategy drugiGeneral, int numberOfMoves) - wywołuje walkę na wojskach przeciwnika.
    - dodaj gettery i settery do wojska.
    - generałowie powinni przechowywac listę

    Stwórz generała:
    RightSideGeneral - który ma być implementowany przez jedną osobę
    LeftSideGeneral - który ma być implementowany przez drugą osobę
Podpowiedź - zaimplementuj metodę walki inaczej niż przeciwnik. spróbuj go "ograć". np. posortuj jego żołnieży rosnąco względem życia i atakuj na początek tych osłabionych.
Po zabiciu żołnierza przeciwnika należy go usunac z listy.
4. Stwórz klasę Arena która w konstruktorze tworzy 1 generała z jednej strony, a następnie 1 generała z 2 strony.
    ma metodę walki:
public void war() - która w pętli wykonuje atak jednego generała na drugim, a następnie w drugą stronę (po 1 turze).
UWAGA! rozpocznij od metod generowania wojska. Osoba w grupie powinna zaimplementować jednego generała. Po wykonaniu całego zadania porównajcie wojska