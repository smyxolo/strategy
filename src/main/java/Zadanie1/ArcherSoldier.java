package Zadanie1;

public class ArcherSoldier extends AbstractSoldier {




    public ArcherSoldier(int experience_points) {
        super(experience_points);
        this.archeryDefenceLevel = 10;
        this.fightDefenceLevel = 8;
        this.spellDefenceLevel = 7;
        this.soldier_type = SOLDIER_TYPE.ARCHER;
    }
}
