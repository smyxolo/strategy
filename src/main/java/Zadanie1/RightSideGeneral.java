package Zadanie1;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class RightSideGeneral implements IGeneralStrategy {

    private int experienceGathered = 1000;
    private List<AbstractSoldier> army;

    public RightSideGeneral(int experiencePerSoldier) {
        this.army = generateSoldiers(experiencePerSoldier);
    }

    @Override
    public List<AbstractSoldier> generateSoldiers(int experiencePoints) {
        Random random = new Random();
        List<AbstractSoldier> soldierList = new ArrayList<>();
        for (int i = 0; i < experienceGathered / experiencePoints; i++) {
            int randomSoldier = random.nextInt(3);
            switch (randomSoldier) {
                case 0: {
                    soldierList.add(new ArcherSoldier(experiencePoints));
                    break;
                }

                case 1: {
                    soldierList.add(new KnightSoldier(experiencePoints));
                    break;
                }

                case 2: {
                    soldierList.add(new WizardSoldier(experiencePoints));
                    break;
                }
            }
        }
        return soldierList;
    }

    public void buryTheCorpses() {
        List<AbstractSoldier> corpses = new ArrayList<>();

        for (AbstractSoldier soldier : army) {
            if (soldier.health_points <= 0) corpses.add(soldier);
        }
        army.removeAll(corpses);
    }

    @Override
    public void fight(IGeneralStrategy oponentGeneral, int numberOfMoves) {
        int iterator = 0;
        while (oponentGeneral.getArmy().size() > 0
                && army.size() > 0
                && numberOfMoves != iterator) {

            army.get(iterator % army.size()).fight(oponentGeneral.getArmy().get(iterator % oponentGeneral.getArmy().size()));
            buryTheCorpses();
            oponentGeneral.buryTheCorpses();
            if(army.size() == 0 || oponentGeneral.getArmy().size() == 0) throw new AllDeadException("Army defeated.");
            iterator++;
        }
    }

    @Override
    public List<AbstractSoldier> getArmy() {
        return army;
    }
}
