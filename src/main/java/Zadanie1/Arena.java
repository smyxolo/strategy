package Zadanie1;

public class Arena {
    IGeneralStrategy leftSideGeneral;
    IGeneralStrategy rightSideGeneral;

    public Arena(IGeneralStrategy leftSideGeneral, IGeneralStrategy rightSideGeneral) {
        this.leftSideGeneral = leftSideGeneral;
        this.rightSideGeneral = rightSideGeneral;
    }

    public void war(){

        while (true){
            try {
                leftSideGeneral.fight(rightSideGeneral, 1);
                rightSideGeneral.fight(leftSideGeneral, 1);
            } catch (AllDeadException ade) {
                System.out.println("The war is over.");
                if(leftSideGeneral.getArmy().size() > 0){
                    System.out.println("Left Side general won with " + leftSideGeneral.getArmy().size() + " soldiers left.");
                    break;
                }
                else {
                    System.out.println("Right Side general won with " + rightSideGeneral.getArmy().size() + " soldiers left.");
                    break;
                }

            }
        }
    }
}
