package Zadanie1;

public class AllDeadException extends RuntimeException {
    public AllDeadException(String message) {
        super(message);
    }
}
